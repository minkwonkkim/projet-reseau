#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>
#include <netinet/ip.h>
#include <arpa/inet.h>

#define BUFSIZE 256

bool ipv4_udp_socket(int *s){
	*s = socket(AF_INET, SOCK_DGRAM, 0);

	if(*s == -1){
		perror("socket");
		return true;
	}

	return false;
}

bool sendSocketTo(const void *buf, int s, size_t len, struct sockaddr_in serverAddr){
	socklen_t addrLen = sizeof(serverAddr);
	if(sendto(s, buf, len, 0, (struct sockaddr *) &serverAddr, addrLen) < 0){
		perror("sendto");
		return true;
	}
	return false;
}

bool recvSocketFrom(void *buf, int s, size_t len, struct sockaddr_in serverAddr){
	socklen_t addrLen = sizeof(serverAddr);
	if(recvfrom(s, buf, len, 0, (struct sockaddr *) &serverAddr, &addrLen) < 0){
		perror("recvfrom");
		return true;
	}
	return false;
}

bool closeSocket(int *s){
	int errCheck = close(*s) == -1;
	if(errCheck)
		return true;
	return false;
}


void initServ(struct sockaddr_in *serverAddr, uint16_t port){

	(*serverAddr).sin_family = AF_INET;
	(*serverAddr).sin_port = htons(port);
	(*serverAddr).sin_addr.s_addr = inet_addr("127.0.0.1");
}

bool connectToServ(int *udpS, const struct sockaddr_in *serverAddr, socklen_t addrLen){
	if(connect(*udpS, (struct sockaddr *) serverAddr, addrLen) == -1){
		perror("connectToServ : couldn't connect");
		return true;
	}
	return false;
}


struct Coordinate{
	uint16_t x;
	uint16_t y;
};

bool convertToBoat(char charMiss[12], struct Coordinate boat[3]){

	if(charMiss[1] != ',' || charMiss[5] != ',' || charMiss[9] != ','){
		write(1, "Format incorrect ; doit inclure un , entre chaque chiffre\n",58);
		return true;

	} else if(charMiss[3] != ' ' || charMiss[7] != ' ' || (charMiss[11] != ' ' && charMiss[11] != '\n')){
		write(1, "Format incorrect ; doit avoir un espace après chaque entrée\n",60);
		return true;

	} else if(charMiss[0] > 52 || charMiss[2] > 52 || charMiss[4] > 52 || charMiss[6] > 52
		|| charMiss[8] > 52 || charMiss[10] > 52){
		write(1, "Format incorrect ; coord ne peuvent pas dépasser 4\n",52);
		return true;

	}else if((charMiss[0] == charMiss[4] && charMiss[2] == charMiss[6]) || 
		(charMiss[0] == charMiss[8] && charMiss[2] == charMiss[6]) ||
		(charMiss[8] == charMiss[4] && charMiss[10] == charMiss[6])){

		//checks if same coord
		write(1, "Format incorrect ; ne peut pas avoir même coord\n",49);
		return true;
	}

	for(int i = 0 ; i < 3 ; i++){
		boat[i].x = htons((uint16_t) (charMiss[i*4]-48));	
		boat[i].y = htons((uint16_t) (charMiss[(i*4)+2]-48));
	}
	return false;
}

bool convertToMiss(char charMiss[4], struct Coordinate* miss){

	if(charMiss[1] != ','){
		write(1, "Format incorrect ; doit inclure un , entre chaque chiffre\n",58);
		return true;

	} else if(charMiss[3] != '\n'){
		write(1, "Format incorrect ; doit appuyer sur entrée\n",31);
		return true;

	} else if(charMiss[0] > 52 || charMiss[2] > 52){
		write(1, "Format incorrect ; coord ne peuvent pas dépasser 4\n",52);
		return true;
	}
	miss->x = htons((uint16_t) (charMiss[0]-48));	
	miss->y = htons((uint16_t) (charMiss[2]-48));

	return false;
}

void displayGrid(char grid[4][4]){
	for(int i = 0; i < 4; i++){
		for(int j = 0 ; j < 4; j++){
			write(1, "[", 1);
			write(1, &grid[i][j], 1);
			write(1, "]", 1);
		}
		write(1, "\n", 1);
	}
}

void setupGrid(struct Coordinate boat[3], char grid[4][4]){
	for(int i = 0 ; i < 3; i++){
		uint16_t curX = ntohs(boat[i].x);
		uint16_t curY = ntohs(boat[i].y);
		grid[curX-1][curY-1] = 'O';
	}
}

struct Missile{
	struct Coordinate coord;
	uint16_t hit; //0 for false, 1 for true
};

int main(){

	int udpS;
	uint16_t port = 2021;
	uint16_t winState = htons((uint16_t)0);
	uint16_t nbPlayer = htons((uint16_t)0);
	struct Missile miss;


	char gridPlayer[4][4] = {
								{' ', ' ', ' ', ' '},
								{' ', ' ', ' ', ' '},
								{' ', ' ', ' ', ' '},
								{' ', ' ', ' ', ' '},
							};
	char gridEnemy[4][4] = {
								{' ', ' ', ' ', ' '},
								{' ', ' ', ' ', ' '},
								{' ', ' ', ' ', ' '},
								{' ', ' ', ' ', ' '},
							};

	struct sockaddr_in serverAddr;

	initServ(&serverAddr, port);
	//socklen_t addrLen = sizeof((serverAddr));

	if(ipv4_udp_socket(&udpS))
		return EXIT_FAILURE;

	//connecting to serv
	if(sendSocketTo(&nbPlayer, udpS, sizeof(uint16_t), serverAddr))
		return EXIT_FAILURE;
	//receiv current player in serv
	if(recvSocketFrom(&nbPlayer, udpS, sizeof(uint16_t), serverAddr))
		return EXIT_FAILURE;

	//if current player in serv 1 then change port in case adress is the same.
	//if adress is same => adress+port combination not unique and everything
	//sent from player1 will be sent to player2
	if(ntohs(nbPlayer) == 1) {
		//no need to get value since this write is "raw", will never cause any issue since only used to print on console and not used by user
		write(1,"Already one player connected, changing port...\n", 47);
		port = 2022;
		initServ(&serverAddr, port);
		//resend confirmation that they changed port to server to bind
		if(sendSocketTo(&nbPlayer, udpS, sizeof(uint16_t), serverAddr))
			return EXIT_FAILURE;

		//waiting for server confirmation and new NbPlayer
		if(recvSocketFrom(&nbPlayer, udpS, sizeof(uint16_t), serverAddr))
			return EXIT_FAILURE;
		write(1,"Changed port !\n", 15);
	}else{ 
	//if current player in serv is 0 then waiting for second player
		write(1,"waiting for other player...\n", 28);
		if(recvSocketFrom(&nbPlayer, udpS, sizeof(uint16_t), serverAddr))
			return EXIT_FAILURE;
		while(ntohs(nbPlayer) < 2){;
			if(recvSocketFrom(&nbPlayer, udpS, sizeof(uint16_t), serverAddr))
				return EXIT_FAILURE;
		}
	}

	//getting boat coords
	char charCordBoat[12];
	struct Coordinate boat[3];

	//waiting for server approval
	write(1,"All Players connected, wait for server..\n", 41);
	if(recvSocketFrom(&nbPlayer, udpS, sizeof(uint16_t), serverAddr))
		return EXIT_FAILURE;

	//no need to get value of this syscall since this write is "raw", will never cause any issue since only used to print on console and not used by user
	write(1,"Good to go, please enter coordinate of your boat x,y separated by a ,\n", 70);
	write(1,"Each coordinate must be separated by a space\n", 45);

	//read user input for boat until player enters correct format

	bool formatCorrect = false;

	while(!formatCorrect){
		if(read(0,charCordBoat, 12*sizeof(char)) < -1)
			return EXIT_FAILURE;

		//converting string into coordinates
		if(!convertToBoat(charCordBoat, boat)){
			formatCorrect = true;
			write(1,"Coordinates entered !\n", 23);
		}
	}

	//send boat
	if(sendSocketTo(boat, udpS, sizeof(struct Coordinate)*3, serverAddr))
		return EXIT_FAILURE;

	setupGrid(boat, gridPlayer);

	//if 0 then defend, if 1 then attack
	uint16_t turn;

	while(ntohs(winState) == 0){
		write(1, "----------Your grid----------\n",30);
		displayGrid(gridPlayer);
		write(1, "-----------Your Enemy---------\n",31);
		displayGrid(gridEnemy);

		//waiting for turn
		write(1,"Wait for your turn !\n", 21);
		if(recvSocketFrom(&turn, udpS, sizeof(uint16_t), serverAddr))
			return EXIT_FAILURE;

		if(ntohs(turn) == 0){
			write(1,"It's your turn, enter coordinates separated by a , !\n", 53);

			//read user input for missile until players enters correct format
			formatCorrect = false;
			struct Coordinate missile;
			char charCoordMiss[4];
			while(!formatCorrect){
				if(read(0,charCoordMiss, 4*sizeof(char)) < -1)
					return EXIT_FAILURE;

				//converting string into coordinates
				if(!convertToMiss(charCoordMiss, &missile)){
					//send missile
					if(sendSocketTo(&missile, udpS, sizeof(struct Coordinate), serverAddr))
						return EXIT_FAILURE;
					//waiting to see if missile is correct and not in history
					uint16_t formatCorrectToSend = htons(0);
					if(recvSocketFrom(&formatCorrectToSend, udpS, sizeof(uint16_t), serverAddr))
						return EXIT_FAILURE;
					formatCorrect = (bool) ntohs(formatCorrectToSend);
					if(formatCorrect)
						write(1,"Coordinates entered !\n", 23);
					else
						write(1, "Coordinate already entered before, try again\n",45);
				}
			}

			//wait for server to tell if missile hit
			if(recvSocketFrom(&miss, udpS, sizeof(struct Missile), serverAddr))
				return EXIT_FAILURE;
			
			char x = (char) (ntohs(miss.coord.x)+'0');
			char y = (char) (ntohs(miss.coord.y)+'0');
			
			if(ntohs(miss.hit) == 1){
				gridEnemy[ntohs(miss.coord.x)-1][ntohs(miss.coord.y)-1] = 'X';
				write(1,"You hit the boat at coordinate : ", 33);
			}else{
				gridEnemy[ntohs(miss.coord.x)-1][ntohs(miss.coord.y)-1] = '~';
				write(1,"You didn't hit the boat at coordinate : ", 40);
			}
			write(1,&x, 1);
			write(1," | ", 3);
			write(1,&y, 1);
			write(1,"\n", 1);

			//waiting for winstate, if changed, then won
			if(recvSocketFrom(&winState, udpS, sizeof(uint16_t), serverAddr))
				return EXIT_FAILURE;
			if(ntohs(winState) != 0)
				write(1,"You won !\n", 10);
		}else{
			//wait for server to tell if missile hit
			if(recvSocketFrom(&miss, udpS, sizeof(struct Missile), serverAddr))
				return EXIT_FAILURE;
			//if missile hit, display life left
			char x = (char)(ntohs(miss.coord.x)+'0');
			char y = (char)(ntohs(miss.coord.y)+'0');
			
			if(ntohs(miss.hit) == 1){
				gridPlayer[ntohs(miss.coord.x)-1][ntohs(miss.coord.y)-1] = 'X';
				write(1,"Your boat got hit at the coordinate : ", 38);
			}else{
				gridPlayer[ntohs(miss.coord.x)-1][ntohs(miss.coord.y)-1] = '~';
				write(1,"Your opponent missed at coordinate : ", 37);
			}
			write(1,&x, 1);
			write(1," | ", 3);
			write(1,&y, 1);
			write(1,"\n", 1);

			//waiting for winstate, if changed, then lost
			if(recvSocketFrom(&winState, udpS, sizeof(uint16_t), serverAddr))
				return EXIT_FAILURE;
			if(ntohs(winState) != 0)
				write(1,"You lost !\n", 11);
		}

	}


	if(closeSocket(&udpS))
		return EXIT_FAILURE;

	return EXIT_SUCCESS;

}
