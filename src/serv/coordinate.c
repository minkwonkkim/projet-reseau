#include "coordinate.h"
#include <stdlib.h>

bool checkCoord(struct Coordinate c1[3], struct Coordinate c2){
	if(c1 == NULL){
		//lserrmsgf("%s\n", "checkCoord : c1 null");
		return false;
	}
	return 	(c1[0].x == c2.x && c1[0].y == c2.y) ||
			(c1[1].x == c2.x && c1[1].y == c2.y) ||
			(c1[2].x == c2.x && c1[2].y == c2.y);

}

bool setCoord(struct Coordinate* c, uint16_t x, uint16_t y){
	if(c == NULL){
//		errmsgf("%s\n", "In setCoord : Coordinate null");
		return true;
	}
	(*c).x = x;
	(*c).y = y;
	return false;
}
