#include "player.h"
#include <stdlib.h>
#include <stdbool.h>
#include <stdio.h>

bool initPlayer(struct Player* newP){
	for(int i = 0; i < 4; i++){
		for(int j = 0; j < 4; j++)
			(*newP).missileHistory[i][j] = 0;
	}
	(*newP).life = 3;
	return false;

}

bool checkHit(struct Missile* miss, struct Player* p){
	//resetting missile before checking
	miss->hit = htons(0);
	
	if(checkCoord(p->boat, miss->coord)){
		(p->life)--;
		miss->hit = htons(1);
		return false;
	}
	return false;
}