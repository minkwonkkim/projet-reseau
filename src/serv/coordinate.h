#ifndef COORDINATE_H
#define COORDINATE_H
#include <stdint.h>
#include <stdbool.h>

struct Coordinate{

	uint16_t x;
	uint16_t y;

};

bool checkCoord(struct Coordinate c1[3], struct Coordinate c2);

bool setCoord(struct Coordinate* c, uint16_t x, uint16_t y);

#endif