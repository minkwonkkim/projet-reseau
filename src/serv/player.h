#ifndef PLAYER_H
#define PLAYER_H

#include <stdint.h>
#include "coordinate.h"
#include <sys/types.h>
#include <unistd.h>
#include <netinet/ip.h>

//comme convenu avec le professeur, un seul bateau de longueur 3
struct Player{
	int udpS;
	struct sockaddr_in serverAddr;
	struct Coordinate boat [3];
	uint16_t missileHistory [4][4];
	uint16_t life;
};

struct Missile{
	struct Coordinate coord;
	uint16_t hit; //0 for false, 1 for true
};


bool checkHit(struct Missile* miss, struct Player* p);
bool initPlayer(struct Player* newP);

#endif