#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>
#include <netinet/ip.h>
#include <arpa/inet.h>
#include "game.h"
#include "player.h"

#define BUFSIZE 256

bool ipv4_udp_socket(int *s){
	*s = socket(AF_INET, SOCK_DGRAM, 0);

	if(*s == -1){
		perror("socket");
		return true;
	}

	return false;
}

bool sendSocketTo(const void *buf, int s, size_t size_buf, struct sockaddr_in serverAddr, struct sockaddr_in* serverAddrcp){
	socklen_t addrLen = sizeof(serverAddr);
	if(sendto(s, buf, size_buf, 0, (struct sockaddr *) &serverAddr, addrLen) < 0){
		perror("sendto");
		return true;
	}
	*(serverAddrcp) = serverAddr;
	return false;
}

bool recvSocketFrom(void *buf, int s, struct sockaddr_in* serverAddr){
	socklen_t addrLen = sizeof(serverAddr);
	if(recvfrom(s, buf, (size_t) (sizeof(struct Coordinate)*3), 0, (struct sockaddr *) &(*serverAddr), &addrLen) < 0){
		perror("recvfrom");
		return true;
	}
	return false;
}

bool closeSocket(int *s){
	int errCheck = close(*s) == -1;
	if(errCheck)
		return true;
	return false;
}

void initServ(struct sockaddr_in *serverAddr, uint16_t port){

	(*serverAddr).sin_family = AF_INET;
	(*serverAddr).sin_port = htons(port);
	(*serverAddr).sin_addr.s_addr = inet_addr("127.0.0.1");
}

bool sendToAllPlayers(void* buf, size_t size_buf, struct Player players[2]){
	bool check = false;

	for(int i = 0; i < 2; i++)
		check = check || sendSocketTo(buf, players[i].udpS, size_buf, 
			(players[i].serverAddr), &(players[i].serverAddr));

	return check;
}

int main(){
	void * buffer[BUFSIZE];
	struct Player players[2];
	uint16_t port = 2021;
	uint16_t nbPlayers = htons((uint16_t)0);
	uint16_t winState = htons((uint16_t)0);
	uint16_t sendingAttackTurn = htons((uint16_t)0);
	uint16_t sendingDefenseTurn = htons((uint16_t)1);

	//setup the two players with 2 different ports 2021 and 2022
	for(uint16_t i = 0 ; i < 2 ; i++){
		initServ(&(players[i].serverAddr), (uint16_t) (port+i)); //the port is +1 if player2

		if(ipv4_udp_socket(&(players[i].udpS))){
			perror("ipv4_udp_socket");
			return EXIT_FAILURE;
		}

		if(bind(players[i].udpS, (struct sockaddr *) &(players[i].serverAddr), 
			(socklen_t)sizeof(players[i].serverAddr))){
			perror("bind");
			return EXIT_FAILURE;
		}
		initPlayer(&(players[i]));
	}

	//waiting for player1
	if(recvSocketFrom(buffer, players[0].udpS, &(players[0].serverAddr)))
		return EXIT_FAILURE;
	//player1 connected, sending nbPlayers
	if(sendSocketTo(&nbPlayers, players[0].udpS, sizeof(uint16_t), 
		(players[0].serverAddr), &(players[0].serverAddr)))
		return EXIT_FAILURE;

	//increasing nbPlayers connected
	nbPlayers = htons((uint16_t)1);
	
	//waiting for player2
	if(recvSocketFrom(buffer, players[0].udpS, &(players[1].serverAddr)))
		return EXIT_FAILURE;
	//player2 connected sending nbPlayers
	if(sendSocketTo(&nbPlayers, players[0].udpS, sizeof(uint16_t), 
			(players[1].serverAddr), &(players[1].serverAddr)))
		return EXIT_FAILURE;
	//player2 received nbPlayers, waiting for confirmation that port has changed
	if(recvSocketFrom(buffer, players[1].udpS, &(players[1].serverAddr)))
		return EXIT_FAILURE;

	//player2 succefully setup, increasing nbPlayers connected and sending new
	//number to all players
	nbPlayers = htons((uint16_t)2);

	if(sendToAllPlayers(&nbPlayers, sizeof(uint16_t), players))
		return EXIT_FAILURE;

	//telling player in order that can enter coordinate, then receiving
	//their boats and stocking them in player struct

	for(int i = 0; i < 2; i++){
		//telling player to send
		if(sendSocketTo(&nbPlayers, players[i].udpS, sizeof(uint16_t), 
				(players[i].serverAddr), &(players[i].serverAddr)))
			return EXIT_FAILURE;
		//rcv boat from player
		if(recvSocketFrom(buffer, players[i].udpS, &(players[i].serverAddr)))
			return EXIT_FAILURE;
		//stocking boat in player struct
		for(int j = 0; j < 3; j++){
			players[i].boat[j].x = ntohs((((struct Coordinate*) buffer)[j]).x);
			players[i].boat[j].y = ntohs((((struct Coordinate*) buffer)[j]).y);
		}
	}

	//setting up first player to play
	uint16_t currentPlayerIndex = 0;
	struct Player currentPlayer = players[currentPlayerIndex];	
	uint16_t currentEnemyIndex = 1;
	struct Player currentEnemy = players[currentEnemyIndex];
	struct Missile checkMiss;
	checkMiss.hit = htons(0);
	
	//loop game begins
	while(ntohs(winState) == 0){

		//sending confirmation that current player can play
		if(sendSocketTo(&sendingAttackTurn, currentPlayer.udpS, sizeof(uint16_t), (currentPlayer.serverAddr), &(currentPlayer.serverAddr)))
			return EXIT_FAILURE;
	
		//sending confirmation that current enemy should be waiting
		if(sendSocketTo(&sendingDefenseTurn, currentEnemy.udpS, sizeof(uint16_t), (currentEnemy.serverAddr), &(currentEnemy.serverAddr)))
			return EXIT_FAILURE;
	
		//receiving missile from player then stocking it until not in history of player's
		//moves
		uint16_t buf;
		do{
			if(recvSocketFrom(buffer, currentPlayer.udpS, &(currentPlayer.serverAddr)))
				return EXIT_FAILURE;
			//stocking in checkMiss
			checkMiss.coord.x = ntohs(((struct Coordinate*) buffer)->x);
			checkMiss.coord.y = ntohs(((struct Coordinate*) buffer)->y);

			//if missile in history, asking to client to send again
			if((players[currentPlayerIndex]).missileHistory[checkMiss.coord.x-1][checkMiss.coord.y-1] == 1)
				buf = htons(0);
			else
				buf = htons(1);
				
			if(sendSocketTo(&buf, currentPlayer.udpS, sizeof(uint16_t), (currentPlayer.serverAddr), &(currentPlayer.serverAddr)))
				return EXIT_FAILURE;

		}while((players[currentPlayerIndex]).missileHistory[checkMiss.coord.x-1][checkMiss.coord.y-1] == 1);
		
		//put missile in history list
		players[currentPlayerIndex].missileHistory[checkMiss.coord.x-1][checkMiss.coord.y-1] = 1;

		//checking if missile hit
		checkHit(&checkMiss, &(players[currentEnemyIndex]));

		//checking health of player
		if(players[currentEnemyIndex].life <= 0){
			winState = htons((uint16_t)1);
		}

		//converting back to h before sending
		checkMiss.coord.x = ((struct Coordinate*) buffer)->x;
		checkMiss.coord.y = ((struct Coordinate*) buffer)->y;

		//telling both player if missile hit by sending missile struct containing
		//hit marker
		if(sendToAllPlayers(&checkMiss, sizeof(struct Missile), players))
			return EXIT_FAILURE;

		//updating the winState
		if(sendToAllPlayers(&winState, sizeof(struct Missile), players))
			return EXIT_FAILURE;

		//changing attacker and defender
		if(currentPlayerIndex == 0){
			currentPlayerIndex = 1;	
			currentEnemyIndex = 0;
		}else{
			currentPlayerIndex = 0;	
			currentEnemyIndex = 1;
		}
		currentPlayer = players[currentPlayerIndex];	
		currentEnemy = players[currentEnemyIndex];
	}


	if(closeSocket(&(players[0].udpS)))
		return EXIT_FAILURE;
	if(closeSocket(&(players[1].udpS)))
		return EXIT_FAILURE;

	return EXIT_SUCCESS;

}
