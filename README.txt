Projet : Petit jeu bataille navale sur un grid 4x4

Comment compiler : (Sous LINUX)
- Décompresser fichier
- Naviguer dans le dossier src puis serv, puis taper make pour compiler
- Executer ./serv
- Ouvrir un 2ème terminal
- Naviguer dans le dossier src puis taper make pour compiler client
- ./client pour executer 1 client
- refaire ./client dans un 3ème terminal

Comment tester :
- C'est une simple bataille navales ; Les indications sont écrits sur la console.
- Il y a un système d'historique (on ne peut pas entrer 2 fois les mêmes coordonnées pour
les missiles)
- Il y a un système de vérification pour les coordonnées du bateau
